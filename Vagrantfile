# vim: filetype=ruby
Vagrant.configure('2') do |config|
  if Vagrant.has_plugin?('vagrant-cachier')
    config.cache.scope = :box
    config.cache.synced_folder_opts = {
      owner: '_apt',
      group: '_apt',
      mount_options: %w(dmode=777 fmode=666)
    }
  end

  config.vm.box = 'ubuntu/xenial64'
  config.vm.hostname = 'lineage-dev'
  config.ssh.forward_agent = true

  config.vm.provider 'virtualbox' do |provider, _override|
    provider.name = 'lineage-dev'
    provider.customize ['modifyvm', :id, '--cpus', '8']
    provider.customize ['modifyvm', :id, '--ioapic', 'on']
    provider.customize ['modifyvm', :id, '--memory', 8 * 1024]
    provider.customize ['modifyvm', :id, '--uartmode1', 'disconnected']
    provider.customize ['modifyvm', :id, '--usb', 'on']
    provider.customize ['modifyvm', :id, '--usbxhci', 'on']
    provider.customize ['usbfilter', 'add', '0', '--target', :id,
      '--name', 'us996', '--vendorid', '0x1004']
    provider.customize ['usbfilter', 'add', '0', '--target', :id,
      '--name', 'fastboot', '--vendorid', '0x18d1']

    # Vagrant really just won't tell me where this path is.
    vm_path = "#{ENV['HOME']}/VirtualBox VMs/lineage-dev"
    disk = "#{vm_path}/disk.vdi"
    swap = "#{vm_path}/swap.vdi"

    unless File.exist?(disk)
      provider.customize ['createhd', '--filename', disk, '--size', 96 * 1024]
    end

    unless File.exist?(swap)
      provider.customize ['createhd', '--filename', swap, '--size', 4 * 1024]
    end

    provider.customize ['storageattach', :id, '--storagectl', 'SCSI',
      '--port', 2, '--device', 0, '--type', 'hdd', '--medium', disk]

    provider.customize ['storageattach', :id, '--storagectl', 'SCSI',
      '--port', 3, '--device', 0, '--type', 'hdd', '--medium', swap]
  end

  if File.exist? "#{ENV['HOME']}/.gitconfig"
    config.vm.provision :file, source: '~/.gitconfig', destination: '.gitconfig'
  end

  if File.exist? "#{ENV['HOME']}/.vimrc"
    config.vm.provision :file, source: '~/.vimrc', destination: '.vimrc'
  end

  if Dir.exist? "#{ENV['HOME']}/.vim"
    config.vm.provision :shell, privileged: false, inline: 'rm -rf ~/.vim'
    config.vm.provision :file, source: '~/.vim', destination: '.vim'
  end

  config.vm.provision :shell do |shell|
    shell.privileged = false
    shell.keep_color = true
    shell.path = 'provisioning/vagrant'
  end
end
